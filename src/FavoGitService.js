import RemoteApi from './api/RemoteApi';
import Profile from "./model/Profile";
import MemoryStorage from "./storage/MemoryStorage";

export default class FavoGitService {
    constructor(dependencies = {}) {
        this.api = dependencies.api || new RemoteApi;
        this.storage = dependencies.storage || new MemoryStorage();
    }

    /**
     * @param {string} login
     * @returns Profile
     */
    favorite(login) {
        return this.profile(login).toggleFavorite();
    }

    favorites() {
        return this.storage.favorites();
    }

    /**
     *
     * @param login
     * @param sources
     * @returns Profile
     */
    profile(login, ...sources) {
        if (this.storage.has(login)) {
            return this.storage.get(login).merge(...sources);
        }

        let profile = new Profile({login}, this.api.profile(login));

        return this.storage.put(profile.merge(...sources));
    }

    search(term, options = {}) {
        return this.api
            .search(term, options)
            .then(result => result.data) // FIXME handle non-200 responses
            .then(result => ({
                total_count: result.total_count,
                profiles: (result.items || []).map((result) => this.profile(result.login, result))
            }));
    }

    clearStorage() {
        this.storage.clear();
    }
}
