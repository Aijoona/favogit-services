class Profile {
    constructor(...sources) {
        this.errors = [];

        this.pending = [];
        this.loaded = [];

        this.merge(...sources);
    }

    merge(...sources) {
        sources.forEach(source => this.addSource(source));

        return this;
    }

    get(prop) {
        let loaded = this.loaded.concat().reverse();

        for (let i = 0; i < loaded.length; i++) {
            if (prop in loaded[i]) {
                return loaded[i][prop];
            }
        }

        return null;
    }

    toMap() {
        let loaded = this.loaded;
        let map = {
            loading: this.loading(),
            incomplete: this.isIncomplete()
        };

        for (let i = 0; i < loaded.length; i++) {
            for (let prop in loaded[i]) {
                if (loaded[i].hasOwnProperty(prop)) {
                    map[prop] = loaded[i][prop];
                }
            }
        }

        return map;
    }

    loading() {
        return this.pending.length !== 0;
    }

    ready() {
        return !this.loading();
    }

    isIncomplete() {
        return this.errors.length > 0;
    }

    then(onfulfill, onerror) {
        return Promise.all(this.pending).then(onfulfill, onerror);
    }

    catch(handler) {
        return Promise.all(this.pending).catch(handler);
    }

    addSource(source) {
        if (!source.then) {
            this.loaded.push(source);
        } else {
            this.pending.push(source);

            source
                .then((result) => this.resolvePendingSource(source, result))
                .catch(error => this.errors.push(error))
        }
    }

    resolvePendingSource(source, result) {
        let index = this.pending.indexOf(source);

        this.pending.splice(index, 1);

        this.loaded.push(result);
    }

    toggleFavorite() {
        let currentValue = this.favorite;

        this.merge({
            favorite: !currentValue,
            favorited_at: +new Date
        });

        return this;
    }
}


let handler = {
    get(target, name) {
        return name in target ? target[name] : target.get(name);
    }
};


export default (...sources) => {
    return new Proxy(new Profile(...sources), handler);
}