import axios from 'axios';

const DEFAULT_PAGE_SIZE = 20;

const api = axios.create({
    baseURL: 'https://api.github.com/',
});

const get = (...params) =>
    api
        .get(...params)
        .catch((error) => {
            let response = error.response;
            let message = 'Something went wrong, please call Batman.';

            if (response && response.status === 403) {
                message = '403: ' + response.data.message;
            }

            return Promise.reject(message);
        });

export default class RemoteApi {
    constructor(token) {
        if (token) {
            api.defaults.headers.common.Authorization = 'token ' + token;
        }
    }

    search(term, options = {}) {
        return get('/search/users', {
            params: Object.assign({q: term, per_page: DEFAULT_PAGE_SIZE}, options)
        })
    }

    profile(login) {
        return get('/users/' + login).then((response) => response.data)
    }
}
