export default class MemoryStorage {
    constructor() {
        this.clear();
    }

    put(profile) {
        this.stash[profile.login] = profile;

        return profile;
    }

    get(login) {
        return this.stash[login];
    }

    has(login) {
        return login in this.stash;
    }

    all() {
        return Object.values(this.stash);
    }

    size() {
        return this.all().length;
    }

    favorites() {
        return this.all().filter((profile) => profile.favorite);
    }

    clear() {
        this.stash = {};
    }
}