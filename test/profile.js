import assert from 'assert';
import {default as createProfile} from '../src/model/Profile';

describe('Profile', function () {

    describe('model creation', function () {
        it('should return an object', () => {
            assert(typeof createProfile() === 'object')
        });

        it('should not be null', () => {
            assert(createProfile() !== null)
        });

        it('should allow mixed sources', () => {
            assert(createProfile({foo: 'bar'}, Promise.resolve({bar: 'foo'})) !== null)
        });
    });

    describe('model api', function () {
        it('should have a `loading` method', () => {
            assert(typeof createProfile().loading === 'function')
        });

        it('should have a `isIncomplete` method', () => {
            assert(typeof createProfile().isIncomplete === 'function')
        });

        it('should have a `merge` method', () => {
            assert(typeof createProfile().merge === 'function')
        });

        it('should have a `toMap` method', () => {
            assert(typeof createProfile().toMap === 'function')
        });

        it('should have a `toggleFavorite` method', () => {
            assert(typeof createProfile().toggleFavorite === 'function')
        });

        it('should be thenable', () => {
            assert(typeof createProfile().then === 'function')
        });
    });

    describe('model static sources', function () {
        it('should forward properties to its static sources', () => {
            assert(createProfile({foo: 'bar'}).foo === 'bar');
            assert(createProfile({foo: 'bar'}).bar === null);
            assert(createProfile({foo: 'bar'}, {bar: 'foo'}).bar === 'foo')
        });

        it('should be ready if there is no promixe sources', () => {
            assert(createProfile({foo: 'bar'}).ready() === true);
        });

        it('should allow `false` value', () => {
            assert(createProfile({foo: false}).foo === false);
        });

        it('should allow overriding properties', () => {
            let profile = createProfile({foo: 'bar'});

            assert(profile.foo === 'bar');

            profile.merge({foo: 'baz'});

            assert(profile.foo === 'baz');
        });
    });

    describe('model promise sources', function () {
        it('should be loading if has at least one promise source', () => {
            let promise = new Promise(function (resolve, reject) {
            });

            assert(createProfile(promise).loading() === true)
        });

        it('should be ready when promise fulfills', () => {
            let manualResolve;
            let promise = new Promise(function (resolve, reject) {
                manualResolve = resolve;
            });
            let profile = createProfile(promise);

            assert(profile.loading() === true);
            manualResolve();

            setTimeout(() => assert(profile.loading() === false), 0);
        });

        it('should forward properties to its promise results', () => {
            let manualResolve;
            let promise = new Promise(function (resolve, reject) {
                manualResolve = resolve;
            });
            let profile = createProfile(promise);

            assert(profile.loading() === true);
            manualResolve({foo: 'bar'});

            setTimeout(() => assert(profile.foo === 'bar'), 0);
        });

        it('should forward properties to its promise results', () => {
            let manualResolve;
            let promise = new Promise(function (resolve, reject) {
                manualResolve = resolve;
            });
            let profile = createProfile(promise);

            assert(profile.loading() === true);
            manualResolve({foo: 'bar'});

            setTimeout(() => assert(profile.foo === 'bar'), 0);
        });

        it('should mark profile as incomplete when promise fails', () => {
            let manualReject;
            let promise = new Promise(function (resolve, reject) {
                manualReject = reject;
            });
            let profile = createProfile(promise);

            assert(profile.loading() === true);
            manualReject({message: 'Something went wrong'});

            setTimeout(() => assert(profile.isIncomplete() === true), 0);
        });
    });

    describe('model #toMap', () => {
        it('should have default properties', () => {
            let profile = createProfile();

            assert.deepEqual(profile.toMap(), {
                incomplete: false,
                loading: false,
            });
        });

        it('should merge loaded sources', () => {
            let profile = createProfile({foo: 'bar', baz: 'baz'}, {blah: 1, bleh: 3}, {blah: 2});

            assert.deepEqual(profile.toMap(), {
                foo: 'bar',
                baz: 'baz',
                blah: 2,
                bleh: 3,
                incomplete: false,
                loading: false,
            });
        });

        it('should allow overriding initial favorite value', () => {
            let profile = createProfile({favorite: true, favorited_at: +new Date});

            assert(profile.favorite === true);
        });
    });

    describe('profile favoriting', function () {
        it('should allow toggling favorite status', () => {
            let profile = createProfile();

            assert(profile.toggleFavorite().favorite === true);
            assert(profile.toggleFavorite().favorite !== true);
            assert(profile.toggleFavorite().favorite === true);
            assert(profile.toggleFavorite().favorite !== true);

        });

        it('should allow overriding initial favorite value', () => {
            let profile = createProfile({favorite: true, favorited_at: +new Date});

            assert(profile.favorite === true);
        });
    });

    describe('profile proxied properties', function () {
        it('should allow access computed properites', () => {
            let profile = createProfile({foo: 'bar'});

            assert(profile.foo === 'bar');
            assert(profile.foo === profile.get('foo'));
            assert(profile.favorite === profile.get('favorite'));
        });
    });
});
