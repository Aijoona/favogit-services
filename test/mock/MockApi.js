import {default as searchResult} from './search';

const rand = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

// see https://www.eonline.com/news/665900/21-cliche-inspirational-quotes-that-everyone-needs-to-stop-using-immediately
const cheesyBios = [
    "It's not the number of breaths we take, but the number of moments that take our breath away.",
    "Life is what happens to you while you're busy making other plans.",
    "You're only as strong as the drinks you mix, the tables you dance on, and the friends you party with",
    "Live for the nights you'll never remember with the friends you'll never forget",
    "Live, laugh, love.",
    "You have to look through the rain to see the rainbow.",
    "Sing like no one is listening. Love like you've never been hurt. Dance like nobody is watching.",
    "Yesterday is history, tomorrow a mystery and today is a gift. That's why we call it the present.",
    "Keep calm and carry on.",
    "Shoot for the moon. Even if you miss, you'll land among the stars.",
    "You have to kiss a lot of frogs before you find your prince.",
    "Everything happens for a reason.",
    "If life gives you lemons, make lemonade.",
    "You'll find love when you stop looking.",
    "Live every day like it's your last.",
    "It takes more muscles to frown than it does to smile.",
    "Dream as if you'll live forever. Live as if you'll die today.",
    "Real eyes realize real lies.",
    "Life is not about waiting for the storm to pass. It's about learning to dance in the rain.",
    "You miss 100% of the shots you don't take.",
];

export default class MockApi {
    constructor(token) {
        this.token = token;
    }

    search(term, options) {
        term = term.toLowerCase();

        if (term === 'should_fail') {
            return new Promise((resolve, reject) => reject('Rejected!'));
        }

        let items = searchResult.filter((profile) => profile.login.toLowerCase().indexOf(term) !== -1);

        return new Promise(function (resolve, reject) {
            resolve({
                data: {
                    items,
                    total_count: items.length
                }
            });
        });
    }

    profile(login) {
        if (login === 'should_fail') {
            return new Promise((resolve, reject) => reject('Rejected!'));
        }

        return new Promise(resolve => resolve({
            login,
            name: login,
            url: `https://api.github.com/users/${login}`,
            html_url: `https://github.com/${login}`,
            followers_url: `https://api.github.com/users/${login}/followers`,
            following_url: `https://api.github.com/users/${login}/following{/other_user}`,
            gists_url: `https://api.github.com/users/${login}/gists{/gist_id}`,
            starred_url: `https://api.github.com/users/${login}/starred{/owner}{/repo}`,
            subscriptions_url: `https://api.github.com/users/${login}/subscriptions`,
            organizations_url: `https://api.github.com/users/${login}/orgs`,
            repos_url: `https://api.github.com/users/${login}/repos`,
            events_url: `https://api.github.com/users/${login}/events{/privacy}`,
            received_events_url: `https://api.github.com/users/${login}/received_events`,
            type: 'User',
            site_admin: false,
            blog: '',
            location: 'Argentina',
            email: null,
            hireable: true,
            bio: cheesyBios[rand(0, cheesyBios.length * 2)], // Most users dont like cheesy bios
            public_repos: rand(0, 100),
            public_gists: rand(0, 100),
            followers: rand(0, 100),
            following: rand(0, 100),
            created_at: '2010-03-10T21:11:55Z',
            updated_at: '2019-01-05T20:28:08Z'
        }));
    }
}


