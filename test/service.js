import assert from 'assert';
import FavoGitService from '../src/FavoGitService';
import MockApi from "./mock/MockApi";

describe('FavoGitService', () => {
    describe('service creation', function () {
        let favoGitService = new FavoGitService();

        it('should return an object', function () {
            assert(typeof favoGitService === 'object')
        });

        it('should not be null', function () {
            assert(favoGitService !== null)
        });
    });

    describe('FavoGitService api', () => {
        let favoGitService = new FavoGitService();

        it('should have a `favorites` method', function () {
            assert(typeof favoGitService.favorites === 'function')
        });

        it('should have a `profile` method', function () {
            assert(typeof favoGitService.profile === 'function')
        });

        it('should have a `search` method', function () {
            assert(typeof favoGitService.search === 'function')
        });

        it('should have a `clearStorage` method', function () {
            assert(typeof favoGitService.clearStorage === 'function')
        });
    });

    describe('FavoGitService search', () => {
        let favoGitService = new FavoGitService({api: new MockApi()});
        let search = favoGitService.search('thomas');

        it('should return an object', () => {
            assert(typeof search === 'object')
        });

        it('should return a thenable object', () => {
            assert(typeof search.then === 'function')
        });

        it('should have a `total_count` property', (done) => {
            search
                .then((result) => assert('total_count' in result))
                .then(done, done);
        });

        it('should have a `profiles` property', (done) => {
            search
                .then((result) => assert('profiles' in result))
                .then(done, done);
        });

        it('`profiles` property should be an array', (done) => {
            search
                .then((result) => assert(Array.isArray(result.profiles)))
                .then(done, done);
        });

        it('`total_count` property is greater than zero', (done) => {
            search
                .then((result) => assert(result.total_count > 0))
                .then(done, done);
        });

        it('`profiles` property should contain at least one profile (for this known search)', (done) => {
            search
                .then(function (result) {
                    let profile = result.profiles[0];

                    assert(typeof profile === 'object');
                    assert(profile !== null);
                    assert(typeof profile.toggleFavorite === 'function');
                })
                .then(done, done);
        });

        it('should allow read error messages', (done) => {
            let search = favoGitService.search('should_fail');

            search
                .catch(error => assert(error === 'Rejected!'))
                .then(done, done)
        });
    });


    describe('FavoGitService#favorite', () => {
        it('should allow toggle profile\'s favorite status', () => {
            let favoGitService = new FavoGitService({api: new MockApi()});

            assert(favoGitService.favorite('demo-profile').favorite);
            assert(!favoGitService.favorite('demo-profile').favorite);
            assert(favoGitService.favorite('demo-profile').favorite);
        });
    });

});