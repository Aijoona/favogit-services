import assert from 'assert';
import {default as createProfile} from '../src/model/Profile';
import MemoryStorage from '../src/storage/MemoryStorage';

describe('MemoryStorage', function () {
    describe('storage creation', function () {
        it('should be a constructor', function () {
            assert(typeof MemoryStorage === 'function')
        });

        it('should be instantiable', function () {
            assert(typeof new MemoryStorage === 'object')
        });
    });

    describe('storage api', function () {
        let storage = new MemoryStorage();

        it('should have a `put` method', function () {
            assert(typeof storage.put === 'function')
        });

        it('should have a `get` method', function () {
            assert(typeof storage.get === 'function')
        });

        it('should have a `has` method', function () {
            assert(typeof storage.has === 'function')
        });

        it('should have a `put` size', function () {
            assert(typeof storage.size === 'function')
        });

        it('should have a `clear` method', function () {
            assert(typeof storage.clear === 'function')
        });

        it('should have a `favorites` method', function () {
            assert(typeof storage.favorites === 'function')
        });

        it('should have an `all` favorites method', function () {
            assert(typeof storage.all === 'function')
        });

    });

    describe('storage #put', function () {
        it('should increase size after putting a profile', function () {
            let storage = new MemoryStorage();

            assert(storage.size() === 0);

            storage.put('aijoona', createProfile({login: 'aijoona'}));

            assert(storage.size() === 1);
        });

        it('should allow retrieve saved profiles', function () {
            let storage = new MemoryStorage();
            let profile = createProfile({login: 'aijoona'});

            storage.put(profile);

            assert(storage.get(profile.login) === profile)
        });
    });

    describe('storage #has', function () {
        it('should return false for a non-stored profile', function () {
            let storage = new MemoryStorage();

            assert(storage.has('some-random-login') === false);
        });

        it('should return true for a stored profile', function () {
            let storage = new MemoryStorage();
            storage.put(createProfile({login: 'aijoona'}));

            assert(storage.has('aijoona') === true);
        });

        it('should allow retrieve saved profiles', function () {
            let storage = new MemoryStorage();
            let profile = createProfile({login: 'aijoona'});

            storage.put(profile);

            assert(storage.get(profile.login) === profile)
        });
    });

    describe('storage clear', function () {
        it('should clear storage state on #clear call', function () {
            let storage = new MemoryStorage();

            assert(storage.size() === 0);

            storage.put('aijoona', createProfile({login: 'aijoona'}));

            assert(storage.size() === 1);

            storage.clear();

            assert(storage.size() === 0);
        });
    });

    describe('storage #favorites', function () {
        it('should return favorited profiles on call', function () {
            let storage = new MemoryStorage();
            let nonFavoritedProfile = createProfile({login: 'aijoona'});
            let favoritedProfile = createProfile({login: 'madrobby', favorite: true});

            storage.put(nonFavoritedProfile);
            storage.put(favoritedProfile);

            assert(storage.favorites().length === 1);
            assert(storage.favorites()[0] === favoritedProfile);

        });
    });
});