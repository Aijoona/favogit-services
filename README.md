## Install

`git clone git@gitlab.com:Aijoona/favogit-services.git`

## Usage

```javascript
import createService from 'favogit-services';

let services = createService();

services.search('aijoona').then(result => console.log(result))

```

## Testing

`npm test`

## API

### search(term, options) : Promise

### profile(login, ...sources) : Profile

### favorites() : Array

### clearStorage() 